#!/bin/bash
set -e

#add datasets if the databses dir doesn't already exist
if [ ! -d $FUSEKI_BASE/databases ]
then
  echo "Copy databases"
  cp -r $FUSEKI_HOME/databases $FUSEKI_BASE/databases
fi

cp $FUSEKI_HOME/config.ttl $FUSEKI_BASE/config.ttl

exec /docker-entrypoint.sh "$@"
