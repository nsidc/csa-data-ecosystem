var baseQueries = {
  csadata: {
    all: {
      query: 'PREFIX csares: <http://example.org/csares#>\
PREFIX : <http://example.org/csa#>\
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\
PREFIX bibo: <http://purl.org/ontology/bibo/>\
PREFIX foaf: <http://xmlns.com/foaf/0.1/>\
prefix qurs: <http://example.org/qurs#>\
\
SELECT ?subject ?predicate ?object ?slabel ?olabel ?sNodeType ?oNodeType ?plabel ?pdesc ?sdesc ?odesc \
WHERE {\
  {\
    ?subject a qurs:QuestionaireResponse .\
    ?subject ?predicate ?object .\
    OPTIONAL { ?subject rdfs:label ?slabel } .\
    OPTIONAL { ?subject rdfs:comment ?sdesc } . \
    OPTIONAL { ?object rdfs:label ?olabel } .\
    OPTIONAL { ?object rdfs:comment ?odesc } .\
    OPTIONAL { ?object foaf:name ?olabel } .\
    OPTIONAL { ?predicate rdfs:label ?plabel } .\
    OPTIONAL { ?predicate rdfs:comment ?pdesc } .\
    OPTIONAL { ?subject a ?sNodeClass .\
      ?sNodeClass rdfs:label ?sNodeType} .\
    OPTIONAL { ?object a ?oNodeClass .\
      ?oNodeClass rdfs:label ?oNodeType} .\
    FILTER (?predicate in (rdf:type, csares:metadataStandard, csares:ogcStandard,\
      csares:specializedDataStandard, csares:radarProduct, csares:atmMetCliHyProduct, csares:multispectralProduct,\
      csares:opticalProduct, csares:upcomingProduct, csares:eoEcosystemLayer,\
      csares:systemStatus, csares:linkageThemes, csares:cloudType, csares:storageCapacity, csares:platformServices) && \
      (?object not in (qurs:QuestionaireResponse))) .\
  }\
}',
      label: 'All'
    },
    node: {
      query: 'PREFIX csares: <http://example.org/csares#>\
PREFIX : <http://example.org/csa#>\
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\
PREFIX bibo: <http://purl.org/ontology/bibo/>\
PREFIX foaf: <http://xmlns.com/foaf/0.1/>\
prefix qurs: <http://example.org/qurs#>\
\
SELECT ?subject ?predicate ?object ?slabel ?olabel ?sNodeType ?oNodeType ?plabel ?sdesc ?odesc \
WHERE {\
  {\
    ?subject a qurs:QuestionaireResponse .\
    ?subject ?predicate ?object .\
    OPTIONAL { ?subject rdfs:label ?slabel } .\
    OPTIONAL { ?subject rdfs:comment ?sdesc } . \
    OPTIONAL { ?object rdfs:label ?olabel } .\
    OPTIONAL { ?object rdfs:comment ?odesc } .\
    OPTIONAL { ?object foaf:name ?olabel } .\
    OPTIONAL { ?predicate rdfs:label ?plabel } .\
    OPTIONAL { ?subject a ?sNodeClass .\
      ?sNodeClass rdfs:label ?sNodeType} .\
    OPTIONAL { ?object a ?oNodeClass .\
      ?oNodeClass rdfs:label ?oNodeType} .\
    FILTER (?predicate in (rdf:type, csares:metadataStandard, csares:ogcStandard,\
        csares:specializedDataStandard, csares:radarProduct, csares:atmMetCliHyProduct, csares:multispectralProduct,\
        csares:opticalProduct, csares:upcomingProduct, csares:eoEcosystemLayer,\
        csares:systemStatus, csares:linkageThemes, csares:cloudType, csares:storageCapacity, csares:platformServices) && \
      (?object not in (qurs:QuestionaireResponse)) && (?slabel = "[[subjectfilter]]" || ?olabel = "[[subjectfilter]]" || ?subject = "[[subjectfilter]]" || ?object = "[[subjectfilter]]") ) .\
  }\
}',
      label: 'Node'
    },
    link: {
      query: 'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\
      PREFIX org: <http://www.w3.org/ns/org#>\
    PREFIX pdps: <http://example.org/pdes#>\
    SELECT ?subject ?object ?slabel ?olabel ?linkType ?linkComment ?sNodeType ?oNodeType\
      WHERE {\
      ?subject pdps:hasHarvester ?harvestObject .\
      ?harvestObject pdps:harvestFrom ?object .\
      ?subject rdfs:label ?slabel .\
      ?object rdfs:label ?olabel .\
      ?harvestObject pdps:harvestStatus ?linkType\
      OPTIONAL {?harvestObject rdfs:comment ?linkComment } .\
      VALUES ?sNodeType { "Organization" }.\
      VALUES ?oNodeType { "Organization" }.\
      FILTER (?linkType = "[[linktype]]").\
      }',
      label: 'Link Type'
    },
    nodelegend: {
      query: 'PREFIX csares: <http://example.org/csares#>\
  PREFIX : <http://example.org/csa#>\
  PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\
  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\
  PREFIX bibo: <http://purl.org/ontology/bibo/>\
  PREFIX foaf: <http://xmlns.com/foaf/0.1/>\
  prefix qurs: <http://example.org/qurs#>\
  \
  SELECT ?subject ?predicate ?object ?slabel ?olabel ?sNodeType ?oNodeType ?plabel ?sdesc ?odesc \
  WHERE {\
  {\
    ?subject a qurs:QuestionaireResponse .\
    ?subject ?predicate ?object .\
    OPTIONAL { ?subject rdfs:label ?slabel } .\
    OPTIONAL { ?subject rdfs:comment ?sdesc } . \
    OPTIONAL { ?object rdfs:label ?olabel } .\
    OPTIONAL { ?object rdfs:comment ?odesc } .\
    OPTIONAL { ?object foaf:name ?olabel } .\
    OPTIONAL { ?predicate rdfs:label ?plabel } .\
    OPTIONAL { ?subject a ?sNodeClass .\
      ?sNodeClass rdfs:label ?sNodeType} .\
    OPTIONAL { ?object a ?oNodeClass .\
      ?oNodeClass rdfs:label ?oNodeType} .\
    FILTER (?predicate in (rdf:type, csares:metadataStandard, csares:ogcStandard,\
        csares:specializedDataStandard, csares:radarProduct, csares:atmMetCliHyProduct, csares:multispectralProduct,\
        csares:opticalProduct, csares:upcomingProduct, csares:eoEcosystemLayer,\
        csares:systemStatus, csares:linkageThemes, csares:cloudType, csares:storageCapacity, csares:platformServices) && \
      (?object not in (qurs:QuestionaireResponse)) && (?plabel = "[[nodetype]]") ) .\
  }\
  }',
      label: 'Node'
    }
  }
};
