# csa-data-ecosystem

Semantic files describing the CSA data ecosystem and programs for visualizing the data.

## Running the system
Run the following commands, these will start servers that need to stay running

* If previously running `docker-compose down`
* `docker-compose up --build`

## Working on the project

### Prerequisites
*  node.js/npm

### Validating the ttl files
* From express-ecosystem subfolder run `npm run validatettl`

### Workspace Setup
* `cd express-ecosystem`
* Run `npm install` to pull down the project packages

### Linting
* `cd express-ecosystem`
* Run `npm run lint` - NOT YET IMPLEMENTED

### Unit Testing
Not currently implemented, when it is
* `cd express-ecosystem`
* Run `npm test` or `npm test:watch`

### Convert Spreadsheet to TTL
* `cd express-ecosystem`
* `npm install` if you haven't already.
* convert the Spreadsheet to csv file 'express-ecosystem/sbeo_interviews.csv'
* `node scripts/generate_csa_responses.js`
