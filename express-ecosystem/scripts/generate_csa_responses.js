const fs = require('fs');
const csv = require("fast-csv");
const uuidv4 = require('uuid/v4');

const strToId = (str) => {
  return '<' + str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index) {
    return index == 0 ? word.toLowerCase() : word.toUpperCase();
  }).replace(/\s+/g, '') + '>';
}

const csvToData = () => {
  var newData = {}
  var responses = []
  var people = {}
  var orgs = {}
  var currentResponse
  return new Promise((resolve, reject) => {
    csv.parseFile("flat.csv", {headers : ["system", "public", "line", "question", "answer"],
    renameHeaders: true, trim: true})
    .on("error", err => {
      console.error('Could not read from the CSV file: flat.csv');
      reject(err);
    })
    .on("data", data => {
      const currentLine = parseInt(data.line);
      if(typeof currentResponse === "undefined" || currentResponse["system"] !== data.system) {
        if(typeof currentResponse !== "undefined") {
          responses.push(currentResponse)
        }
        currentResponse = {}
        currentResponse["system"] = data.system
        currentResponse["description"] = data.system
        currentResponse["src"] = data.system.replace(/\./g, '').replace(/ /g, '_')
        currentResponse["entries"] = []
      }
      if(currentLine === 1) { //1 - 6 don't exist any longer
        if(typeof currentResponse !== "undefined") {
          responses.push(currentResponse)
        }
        const personId = strToId(data.answer)
        currentResponse["entries"].push(`bibo:interviewer ${personId}`)
        people[personId] = data.answer
      } else if (currentLine === 2) {
        const personId = strToId(data.answer)
        currentResponse["entries"].push(`bibo:interviewee ${personId}`)
        people[personId] = data.answer
      } else if (currentLine === 3) {
        const orgId = strToId(data.answer)
        currentResponse["entries"].push(`qurs:organization ${orgId}`)
        currentResponse["description"] = data.answer
        orgs[orgId] = data.answer
      } else if (currentLine === 5) {
        //skip this line
      } else if (currentLine === 6) {
        currentResponse["entries"].push(`csares:platformName "${data.answer}"`)
        currentResponse["description"] = currentResponse["description"] + ' - ' + data.answer
      } else if(currentLine >= 7 && currentLine <= 10) {
        currentResponse["entries"].push(`csares:eoEcosystemLayer "${data.answer}"`)
      } else if(currentLine == 11) {
        currentResponse["description"] = `${currentResponse["description"]}<br/>Start Date: ${data.answer}`
      } else if(currentLine >= 12 && currentLine <= 14) {
        currentResponse["entries"].push(`csares:systemStatus "${data.answer}"`)
      } else if(currentLine == 15) {
        currentResponse["description"] = `${currentResponse["description"]}<br/>Web Address: ${data.answer}`
      } else if(currentLine == 16) {
        currentResponse["description"] = `${currentResponse["description"]}<br/>Short Description: ${data.answer}`
      } else if (currentLine >= 17 && currentLine <= 22) {
        currentResponse["entries"].push(`csares:linkageThemes "${data.answer}"`)
        //Skipping lines 23-36
      } else if (currentLine >= 37 && currentLine <= 39) {
        currentResponse["entries"].push(`csares:cloudType "${data.answer}"`)
      } else if (currentLine >= 40 && currentLine <= 46) {
        currentResponse["entries"].push(`csares:storageCapacity "${data.answer}"`)
      } else if (currentLine >= 49 && currentLine <= 54) {
        currentResponse["entries"].push(`csares:metadataStandard "${data.answer}"`)
      } else if (currentLine >= 56 && currentLine <= 60) {
        currentResponse["entries"].push(`csares:ogcStandard "${data.answer}"`)
      } else if (currentLine >= 62 && currentLine <= 64) {
        currentResponse["entries"].push(`csares:specializedDataStandard "${data.answer}"`)
        //skipping lines 65-67
      } else if (currentLine >= 68 && currentLine <= 72) {
        currentResponse["entries"].push(`csares:platformServices "${data.answer}"`)
        //skipping 73-76
      } else if (currentLine >= 77 && currentLine <= 82) {
        currentResponse["entries"].push(`csares:radarProduct "${data.answer}"`)
      } else if (currentLine >= 84 && currentLine <= 87) {
        currentResponse["entries"].push(`csares:atmMetCliHyProduct "${data.answer}"`)
      } else if (currentLine >= 89 && currentLine <= 91) {
        currentResponse["entries"].push(`csares:multispectralProduct "${data.answer}"`)
      } else if (currentLine >= 93 && currentLine <= 103) {
        currentResponse["entries"].push(`csares:opticalProduct "${data.answer}"`)
      } else if (currentLine == 107) {
        currentResponse["entries"].push('csares:upcomingProduct csares:upcomingSentinel4')
      } else if (currentLine == 108) {
        currentResponse["entries"].push('csares:upcomingProduct csares:upcomingSentinel5')
      } else if (currentLine == 109) {
        currentResponse["entries"].push('csares:upcomingProduct csares:upcomingSentinel6')
      }
    })
    .on("end", () => resolve({people: people, orgs: orgs, responses: responses}));
  });
}

const responseToTtlString = (r) => {
  let output = '<' + r.src + '> a qurs:QuestionaireResponse ;\n'
  output = output + `  rdfs:label "${r.system}" ;\n`
  output = output + `  rdfs:comment "${r.description}" ;\n`
  output = output + r.entries.map(function(e) {
    return '  ' + e
  }).join(' ;\n')
  output = output + ' .\n\n'
  return output
}

csvToData().then((data) => {
  var ttlOutput = "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n" +
  "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n" +
  "@prefix foaf: <http://xmlns.com/foaf/0.1/> .\n" +
  "@prefix bibo: <http://purl.org/ontology/bibo/> .\n" +
  "@prefix dc: <http://purl.org/dc/elements/1.1/> .\n" +
  "@prefix qurs: <http://example.org/qurs#> .\n" +
  "@prefix csares: <http://example.org/csares#> .\n" +
  "@prefix : <http://example.org/csa> .\n\n"

  //   ttlOutput = ttlOutput + Object.entries(data.people).map(function(arr) {
  //     return arr[0] + ' a foaf:Person ;\n' + '  foaf:name "' + arr[1] + '" .\n\n'
  // }).join('')

  ttlOutput = ttlOutput + Object.entries(data.orgs).map(function(arr) {
    return arr[0] + ' a foaf:Organization ;\n' + '  foaf:name "' + arr[1] + '" .\n\n'
  }).join('')

  ttlOutput = ttlOutput + data.responses.map(responseToTtlString).join('')

  fs.writeFile('../sparql-server/ttl/csaResponses.ttl', ttlOutput, (err) => {
    // throws an error, you could also catch it here
    if (err) throw err;

    // success case, the file was saved
    console.log('Saved ttl file!');
  });
})
